package app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.Certificate;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.mail.internet.MimeMessage;

import org.apache.xml.security.utils.JavaUtils;

import com.google.api.services.gmail.Gmail;

import model.mailclient.Email;
import model.mailclient.MailBody;
import support.MailHelper;
import support.MailWritter;
import util.Base64;
import util.GzipUtil;
import util.IVHelper;
import xml.asymmetric.AsymmetricKeyEncryption;
import xml.document.DOMCreate;
import xml.signature.SignEveloped;

public class WriteMailClient extends MailClient {
	
	private static final String KEY_FILE = "./data/session.key";
	private static final String IV1_FILE = "./data/iv1.bin";
	private static final String IV2_FILE = "./data/iv2.bin";

	public static void main(String[] args) {

		try {
			Gmail service = getGmailService();

			System.out.println("Insert a sender(your email adress):");
			BufferedReader reader2 = new BufferedReader(new InputStreamReader(System.in));
			String userEmail = reader2.readLine();

			final String xmlFilePath = "./data/" + userEmail + "_enc.xml";

			System.out.println("Insert a reciever:");
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String reciever = reader.readLine();

			System.out.println("Insert a subject:");
			String subject = reader.readLine();

			System.out.println("Insert body:");
			String body = reader.readLine();

			//------------------------------------------------KONTROLNA TACKA
//			
//			// Compression
//			String compressedSubject = Base64.encodeToString(GzipUtil.compress(subject));
//			String compressedBody = Base64.encodeToString(GzipUtil.compress(body));
//
//			// Key generation
//			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
//			SecretKey secretKey = keyGen.generateKey();
//			Cipher aesCipherEnc = Cipher.getInstance("AES/CBC/PKCS5Padding");
//
//			// inicijalizacija za sifrovanje
//			IvParameterSpec ivParameterSpec1 = IVHelper.createIV();
//			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec1);
//
//			// sifrovanje
//			byte[] ciphertext = aesCipherEnc.doFinal(compressedBody.getBytes());
//			String ciphertextStr = Base64.encodeToString(ciphertext);
//			System.out.println("Kriptovan tekst: " + ciphertextStr);
//
//			// inicijalizacija za sifrovanje
//			IvParameterSpec ivParameterSpec2 = IVHelper.createIV();
//			aesCipherEnc.init(Cipher.ENCRYPT_MODE, secretKey, ivParameterSpec2);
//
//			byte[] ciphersubject = aesCipherEnc.doFinal(compressedSubject.getBytes());
//			String ciphersubjectStr = Base64.encodeToString(ciphersubject);
//			System.out.println("Kriptovan subject: " + ciphersubjectStr);
//
//			Certificate cert = getCertificate();
//			PublicKey pk = cert.getPublicKey();
//
//			Cipher rsaCipherEnc = Cipher.getInstance("RSA/ECB/PKCS1Padding");
//			rsaCipherEnc.init(Cipher.ENCRYPT_MODE, pk);
//			// kriptovanje
//			byte[] sifrovan = rsaCipherEnc.doFinal(secretKey.getEncoded());
//			System.out.println("Kriptovan text: " + Base64.encodeToString(sifrovan));
//
//			MailBody mb = new MailBody(ciphertextStr, ivParameterSpec1.getIV(), ivParameterSpec2.getIV(), sifrovan);
//
//			// snimaju se bajtovi kljuca i IV.
//			JavaUtils.writeBytesToFilename(KEY_FILE, secretKey.getEncoded());
//			JavaUtils.writeBytesToFilename(IV1_FILE, ivParameterSpec1.getIV());
//			JavaUtils.writeBytesToFilename(IV2_FILE, ivParameterSpec2.getIV());
//			
//			MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, ciphersubjectStr, mb.toCSV());
//------------------------------------------------------------------------------------------------------------------------------
			Email email = new Email(userEmail, subject, body,reciever);

			DOMCreate.createDom(email);

// Potpisivanj poruke------------------------------
			SignEveloped.testIt(email);
			
// Enkriptovanje-----------------------------------
  
			AsymmetricKeyEncryption.testIt(email);
//--------------------------------------------------

			MimeMessage mimeMessage = MailHelper.createMimeMessage(reciever, xmlFilePath);
			MailWritter.sendMessage(service, "me", mimeMessage);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Certificate getCertificate() {
		try {

			FileInputStream is = new FileInputStream(
					"C:\\Users\\tomac\\git\\ibmadrfakr\\Mail Client V2\\data\\userA.jks");
			KeyStore keystore = KeyStore.getInstance("JKS", "SUN");
			String password = "milosnike";
			char[] passwd = password.toCharArray();
			keystore.load(is, passwd);
			String alias = "userb";

			// Get certificate of public key
			Certificate cert = keystore.getCertificate(alias);
			// Get public key
			PublicKey publicKey = cert.getPublicKey();

			String publicKeyString = Base64.encodeToString(publicKey.getEncoded());

			System.out.println(publicKeyString);

			return cert;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
