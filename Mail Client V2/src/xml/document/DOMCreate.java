package xml.document;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.transform.TransformerException;

import model.mailclient.Email;

public class DOMCreate {
	public static String path = "./data/";

	public static void createDom(Email email) throws TransformerException, IOException {
		String putanja = path + email.getReciver() + ".xml";
		try {
			FileOutputStream fos = new FileOutputStream(new File(putanja));
			XMLEncoder encoder = new XMLEncoder(fos);
			encoder.writeObject(email);
			encoder.close();
			fos.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public static Email readDom(Email email) throws IOException {
		String putanja = path + email.getReciver() + ".xml";
		FileInputStream fis = new FileInputStream(new File(putanja));
		XMLDecoder decoder = new XMLDecoder(fis);
		Email m = (Email) decoder.readObject();
		decoder.close();
		fis.close();
		consolePrint(m);
		return m;
	}

	private static void consolePrint(Email m) {
		System.out.println("===================================");
		System.out.println("Email posiljalac: " + m.getReciver());
		System.out.println("Email naslov: " + m.getSubject());
		System.out.println("Email poruka: " + m.getBody());
		System.out.println("====================================");
	}

}
