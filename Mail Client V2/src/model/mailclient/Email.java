package model.mailclient;

public class Email {
	private String reciver;
	private String sender;
	private String subject;
	private String body;

	public Email() {
	}

	public Email(String reciver, String subject, String body,String sender) {
		this.reciver = reciver;
		this.subject = subject;
		this.body = body;
		this.sender=sender;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReciver() {
		return reciver;
	}

	public void setReciver(String reciver) {
		this.reciver = reciver;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
