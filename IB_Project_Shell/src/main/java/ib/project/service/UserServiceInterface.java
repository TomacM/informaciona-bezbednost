package ib.project.service;

import java.util.List;
import ib.project.model.User;

public interface UserServiceInterface {
	
	List<User> findAll();
	User save(User user);
	User findByUsernameAndPassword(String username, String password);
}