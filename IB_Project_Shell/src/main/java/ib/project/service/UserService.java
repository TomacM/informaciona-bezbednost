package ib.project.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ib.project.model.User;
import ib.project.repository.UserRepository;

@Service
public class UserService implements UserServiceInterface {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User findByUsernameAndPassword(String username, String password) {

		return userRepository.findByUsernameAndPassword(username, password);
	}

	@Override
	public ArrayList<User> findAll() {

		return (ArrayList<User>) userRepository.findAll();
	}

	@Override
	public User save(User user) {

		return userRepository.save(user);
	}

	public String activateUser(String username) {

		User u = userRepository.findByUsername(username);
		u.setActive(true);
		userRepository.save(u);

		return u.getUsername();
	}
}
