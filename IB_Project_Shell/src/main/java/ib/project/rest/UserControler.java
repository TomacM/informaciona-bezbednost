package ib.project.rest;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ib.project.model.User;
import ib.project.service.UserService;

@RestController
@RequestMapping(value = "api/ib")
public class UserControler {

	@Autowired
	public UserService userService;

	// Putanja koja pronalazi sve korisnike
	@GetMapping(path = "/")
	public ArrayList<User> findAll() {
		return userService.findAll();
	}

	@GetMapping(value = "/login/{username}/{password}")
	public ResponseEntity<User> login(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		User user = userService.findByUsernameAndPassword(username, password);
		if (user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@PostMapping(path = "/login/{username}/{password}")
	public ResponseEntity<User> loginUser(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		User user = userService.findByUsernameAndPassword(username, password);
		try {
			return new ResponseEntity<User>(user, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(value = "/register/{username}/{password}")
	public ResponseEntity<Void> registerStudent(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		User user = new User();
		user.setActive(false);
		user.setAuthority("USER");
		user.setPassword(password);
		user.setUsername(username);

		userService.save(user);
		return new ResponseEntity<Void>(HttpStatus.OK);

	}

}
