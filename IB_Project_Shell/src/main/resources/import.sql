DROP SCHEMA IF EXISTS informacionabezbednost;
CREATE SCHEMA informacionabezbednost DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE informacionabezbednost;

CREATE TABLE user(
	id INT AUTO_INCREMENT,
    username VARCHAR(35) NOT NULL,
	password VARCHAR(20) NOT NULL, 
	active boolean,
	authority VARCHAR(10) NOT NULL,
    PRIMARY KEY(id)
);

INSERT INTO user (username, password, active, authority) VALUES ('admin@mail.com','admin', 1, 'ADMIN');

